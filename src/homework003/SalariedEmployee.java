package homework003;

public class SalariedEmployee extends StaffMember {
    private double salary;
    private double bonus;

    public SalariedEmployee(int eId, String eName, String eAddress, double eSalary, double eBonus)
    {
        super(eId, eName, eAddress);
        salary=eSalary;
        bonus=eBonus;
    }
    public String toString()
    {
        String result = "id: " + id + "\n";
        result += "Name: " + name  + "\n";
        result += "Address: " + address;
        result += "Salary: " + salary;
        result += "Bonus: " + bonus;
        return result;
    }
    public double pay()
    {
//        double payment = super.pay() + bonus;
        bonus = 0;
//        return payment;
        return 0.0;
    }
}

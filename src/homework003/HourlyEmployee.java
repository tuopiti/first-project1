package homework003;

public class HourlyEmployee extends StaffMember{
    private int hoursWorked;
    private double rate;
    public HourlyEmployee(int eId , String eName, String eAddress, int eHoursWorked, double eRate)
    {
        super(eId, eName, eAddress);
        hoursWorked=eHoursWorked;
        rate=eRate;
        hoursWorked = 0;
    }
    public void addHours(int moreHours)
    {
        hoursWorked += moreHours;
    }
    public double pay()
    {
        double payment = rate * hoursWorked;
        hoursWorked = 0;
        return payment;
    }
    public String toString()
    {
        String result = super.toString();
        result += "\nCurrent hours: " + hoursWorked;
        return result;
    }
}

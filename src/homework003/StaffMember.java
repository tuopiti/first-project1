package homework003;

public abstract class StaffMember {
    protected int id;
    protected String name;
    protected String address;
   
    public StaffMember(int eId,String eName, String eAddress)
    {
        id= eId;
        name = eName;
        address = eAddress;
    }
    public String toString()
    {
        String result = "id: " + id + "\n";
        result += "Name: " + name  + "\n";
        result += "Address: " + address;
        return result;
    }
    public abstract double pay();
//    public abstract int vaykay();
}

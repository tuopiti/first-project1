package homework003;

public class Staff {
    private StaffMember[]	staffList;
  
    public Staff(){
        staffList = new StaffMember[3];
        staffList[0]= new Volunteer(1,"Sok","phnom Penh");
        staffList[1]= new SalariedEmployee(2,"dara","kampong cham",1000,50);
        staffList[2]= new HourlyEmployee(3,"Jimmy","canada",8,70);
//        ((HourlyEmployee) staffList[2]).addHours(40);
    }
   
    public void payday()
    {
        double amount;
        for (int count = 0; count < staffList.length; count++)
        {
            System.out.println(staffList[count]);
            amount = staffList[count].pay(); // polymorphic
            if (amount == 0.0)
                System.out.println("Thanks!");
            else
                System.out.println("Paid: " + amount);
            System.out.println("-----------------------------------");
        }
    }
    public void vaykayList()
    {
        System.out.println("StaffMembers.");
        System.out.println("-----------------------------------");

        int days;
        for (int count = 0; count < staffList.length; count++)
        {
            System.out.println(staffList[count]);
            System.out.println("-----------------------------------");
        }
    }
}
